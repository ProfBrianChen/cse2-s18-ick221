import java.util.Scanner;
public class Yahtzee{
  //this line defines a main method 
  public static void main(String[] args) { 
    //ending main method 
   Scanner myScanner=new Scanner (System.in);
    System.out.println("Enter 1 for a random number Enter 2 for imput number");
 
   int userInput=myScanner.nextInt(); 
    
//part 1: rolling the dice or chosing numbers 
    
   int Roll1=0; //declaring number on the first dice roll 
   int Roll2=0; //declaring number on the second dice roll 
   int Roll3=0; //declaring number on the third dice roll 
   int Roll4=0; //declaring number on the fourth dice roll 
   int Roll5=0; //declaring number on the fifth dice roll 
    
    if (userInput==1){
      Roll1=(int)(Math.random()*6)+1; //random number generator from 1 to 6
      Roll2=(int)(Math.random()*6)+1;
      Roll3=(int)(Math.random()*6)+1;
      Roll4=(int)(Math.random()*6)+1;
      Roll5=(int)(Math.random()*6)+1;
        System.out.println("Your 5 digits are: " +Roll1+ " " +Roll2+ " " +Roll3+ " " +Roll4+ " " +Roll5);
    } //prints out random generated numbers
    
    else if (userInput==2){
      System.out.println("Enter a 5 digit number");
      Roll1=myScanner.nextInt();
      
      if (Roll1>=1 && Roll1<=6){
        Roll2=myScanner.nextInt();} // can input a second number if the frist number is between 1 and 6
      else 
      {System.out.println("error");
      System.exit(0);}
      if (Roll2>=1 && Roll2<=6){
        Roll3=myScanner.nextInt();}
      else 
      {System.out.println("error");
      System.exit(0);}   
      if (Roll3>=1 && Roll3<=6){
        Roll4=myScanner.nextInt();} 
      else 
      {System.out.println("error");
      System.exit(0);}
      if (Roll4>=1 && Roll4<=6){
        Roll5=myScanner.nextInt();}
      else 
      {System.out.println("error");
      System.exit(0);}
      if (Roll5>=1 && Roll5<=6){
        System.out.println("Your 5 digits are:" +Roll1+ " " +Roll2+ " " +Roll3+ " " +Roll4+ " " +Roll5);}
      else {
        System.out.println("error");
        System.exit(0);} 
      }
       
    // Upper Section 
    
    int SumAces=0;
    if (Roll1==1){
      SumAces=SumAces+1;} 
    if (Roll2==1){
      SumAces=SumAces+1;}
    if (Roll3==1){
      SumAces=SumAces+1;}
    if (Roll4==1){
      SumAces=SumAces+1;}
    if (Roll5==1){
      SumAces=SumAces+1;}  //incriments value for SumAces each time player rolls a 1 
    System.out.println("The sum of Aces:" +SumAces);
    
    int SumTwos=0;
    if (Roll1==2){
      SumTwos=SumTwos+1;}
    if (Roll2==2){
      SumTwos=SumTwos+1;}
    if (Roll3==2){
      SumTwos=SumTwos+1;}
    if (Roll4==2){
      SumTwos=SumTwos+1;}
    if (Roll5==2){
      SumTwos=SumTwos+1;} //incriments value for SumTwos
    System.out.println("The sum of Twos:" +SumTwos);
    
    int SumThrees=0;
    if (Roll1==3){
      SumThrees=SumThrees+1;}
    if (Roll2==3){
      SumThrees=SumThrees+1;}
    if (Roll3==3){
      SumThrees=SumThrees+1;}
    if (Roll4==3){
      SumThrees=SumThrees+1;}
    if (Roll5==3){
      SumThrees=SumThrees+1;}
    System.out.println("The sum of Threes:" +SumThrees);
    
    int SumFours=0;
    if (Roll1==4){
      SumFours=SumFours+1;}
    if (Roll2==4){
      SumFours=SumFours+1;}
    if (Roll3==4){
      SumFours=SumFours+1;}
    if (Roll4==4){
      SumFours=SumFours+1;}
    if (Roll5==4){
      SumFours=SumFours+1;} 
    System.out.println("The sum of Fours:" +SumFours);
      
    int SumFives=0;
    if (Roll1==5){
      SumFives=SumFives+1;}
    if (Roll2==5){
      SumFives=SumFives+1;}
    if (Roll3==5){
      SumFives=SumFives+1;}
    if (Roll4==5){
      SumFives=SumFives+1;}
    if (Roll5==5){
      SumFives=SumFives+1;}
    System.out.println("The sum of Fives:" +SumFives);
    
    int SumSixes=0;
    if (Roll1==6){
      SumSixes=SumSixes+1;}
    if (Roll2==6){
      SumSixes=SumSixes+1;}
    if (Roll3==6){
      SumSixes=SumSixes+1;}
    if (Roll4==6){
      SumSixes=SumSixes+1;}
    if (Roll5==6){
      SumSixes=SumSixes+1;}
    System.out.println("The sum of Sixes:" +SumSixes);
    
    int UpperSectionTotal=SumAces+SumTwos+SumThrees+SumFours+SumFives+SumSixes;
    if (UpperSectionTotal>65){
      UpperSectionTotal=UpperSectionTotal+35;} //bonus 
    
    System.out.println("The Upper Section Total is:" +UpperSectionTotal);
    System.out.println("The Upper Section Total with Bonus:" +UpperSectionTotal);
    
   //Lower Section 
    
    int numberAces=SumAces;
    int numberTwos=SumTwos/2; 
    int numberThrees=SumThrees/3;
    int numberFours=SumFours/4;
    int numberFives=SumFives/5;
    int numberSixes=SumSixes/6; 
    
    int ThreeOfAKind=0;
    int FourOfAKind=0;
    int FullHouse=0;
    int SmallStraight=0;
    int LargeStraight=0;
    int Yahtzee=0;
    int Chance=0;
    //scores of each component 
    
    //3 of a kind 
    if (numberAces==3){
      ThreeOfAKind=3;}
    if (numberTwos==3){
      ThreeOfAKind=6;}
    if (numberThrees==3){
      ThreeOfAKind=9;}
    if (numberFours==3){
      ThreeOfAKind=12;}
    if (numberFives==3){
      ThreeOfAKind=15;}
    if (numberSixes==3){
      ThreeOfAKind=18;}
    System.out.println("Three of a kind:" +ThreeOfAKind);
    
    //4 of a kind 
    if (numberAces==4){
      FourOfAKind=4;}
    if (numberTwos==4){
      FourOfAKind=8;}
    if (numberThrees==4){
      FourOfAKind=12;}
    if (numberFours==4){
      FourOfAKind=16;}
    if (numberFives==4){
      FourOfAKind=20;}
    if (numberSixes==4){
      FourOfAKind=24;}
    System.out.println("Four of a kind:" +FourOfAKind);
    
    //Full House 
    //pair of dice match but other 3 do not 
    //value is 25
    if ((numberAces==3 || numberTwos==3 || numberThrees==3 || numberFours==3 || numberFives==3 || numberSixes==3) 
    && (numberAces==2 || numberTwos==2 || numberThrees==2 || numberFours==2 || numberFives==2 || numberSixes==2)){
      FullHouse=25;}
    System.out.println("Full house: " + FullHouse);
    
    //Small Straight 
    //4 dice exhibit sequential values 
    if ((numberAces==1&&numberTwos==1&&numberThrees==1&&numberFours==1)
    ||(numberTwos==1&&numberThrees==1&&numberFours==1&&numberFives==1)
    ||(numberThrees==1&&numberFours==1&&numberFives==1&&numberSixes==1)){
      SmallStraight=30;}
    System.out.println("Small straight: " + SmallStraight);
    
    //Large Straight
    //all 5 dice are sequential 
    if ((numberAces==1&&numberTwos==1&&numberThrees==1&&numberFours==1&&numberFives==1)
         ||(numberTwos==1&&numberThrees==1&&numberFours==1&&numberFives==1&&numberSixes==1)){
      LargeStraight=40;}
     System.out.println("Large straight: " + LargeStraight);
    
    //Yahtzee 
    //all 5 dice are identical 
    if (numberAces==5||numberTwos==5||numberThrees==5||numberFours==5||numberFives==5||numberSixes==5)
      {Yahtzee=50;} 
      System.out.println("Yahtzee: " + Yahtzee);
    
    //Chance
    //Chance is the sum of all values
    Chance=Roll1+Roll2+Roll3+Roll4+Roll5; 
     System.out.println("Chance: " + Chance);
    
    // Lower total 
     int LowerTotal=ThreeOfAKind+FourOfAKind+Yahtzee+FullHouse+SmallStraight+LargeStraight+Chance; 
    // Grand total 
     int GrandTotal=LowerTotal+UpperSectionTotal; 
     
     
     System.out.println("Lower section total: " + LowerTotal); // output of the score on the lower section 
     System.out.println("Grand total: " + GrandTotal); // output of the score on the grand total
    
    
    
    }
      
    }
    
    
    
    
    
    
    
