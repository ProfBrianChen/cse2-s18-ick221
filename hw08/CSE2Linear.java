import java.util.Scanner;
import java.util.Arrays; 
public class CSE2Linear{
  public static void main (String[]args){
    System.out.println("Enter 15 ascending ints for final grades in CSE2 ");
    //put them into an array 
     Scanner input = new Scanner (System.in);
    int counter = 0; 
    int [] array1= new int [15];   //array with 15 inputs 
    while (counter < 15){
      if (input.hasNextInt()){   //checking that input is an integer 
        array1[counter] = input.nextInt();
        if (array1[counter] < 0 || array1[counter] >100) {  //checks if input is out of range 
        System.out.println("Please enter an integer in the range from 0-100");
         // System.exit(0);
        }
        else {
          if (counter > 0){
            while (array1[counter] < array1[counter-1]){
              System.out.println("Enter an integer greater than the last integer ");
           //   System.exit(0);
            }
          }
          counter++;
        }
      }
      else{
        System.out.println("Please enter an integer ");
      //  System.exit(0);
      }
    }
    System.out.println("Sorted: ");
    System.out.println(Arrays.toString(array1));// converts array to a string statement 
    System.out.print("Enter a grade to search for: ");
       if (input.hasNextInt()){
        int x = input.nextInt();
            BinarySearch(array1, x);// calls LinearSearch Method
            Scramble(array1);// calls Scramble Method
            System.out.println("Scrambled:");
            System.out.println(Arrays.toString(array1));
            System.out.print("Enter a grade to search for: ");// tells the user to enter a number to search for
              if (input.hasNextInt()){
                LinearSearch(array1,x);// if user entered an int calls on LinearSearch Method
            }
            else{
                System.out.println("Please enter an integer ");
            //    System.exit(0);
            }
        }
              else {
                System.out.println("Enter a grade to search for ");
                 System.exit(0); 
  }
  } 
  public static void LinearSearch(int[] array1, int x){// LinearSearch method
        for (int i = 0; i < array1.length; i++){
            if (array1[i] == x){
                System.out.println(x + " was found in the list in " + (i + 1) + " iterations");
                break;
            }
            else if (array1[i] != x && i == (array1.length - 1)){//if user input wasn't found after 15 iterations 
                System.out.println(x + " was not found in " + (i + 1) + " iterations");
            }
        }
    }
    public static int[] Scramble(int[] array1){// Scramble method
        for (int i = 0; i < array1.length; i++){
            int x = (int)(Math.random()*array1.length);
            int store = array1[i];// temporarily stores number at i index in array
            while (x != i){// if the random number isn't equal to i, switches index of i with index of random number
            
                array1[i] = array1[x];
                array1[x] = store;
                break;
            }
        }
        return array1;
    }
    public static void BinarySearch(int[] array2, int integer){
        int highIndex = array2.length - 1;
        int lowIndex = 0;
        int counter = 0;
        while (lowIndex <= highIndex) {
            counter++;
            int midIndex = (highIndex + lowIndex)/2;
            if (integer < array2[midIndex]){
                highIndex = midIndex - 1;
            }
            else if (integer > array2[midIndex]){
                lowIndex = midIndex + 1;   
            }
            else if (integer == array2[midIndex]) {
                System.out.println(integer + " was found in " + counter + " iterations");
                break;
            }
        }
        if (lowIndex > highIndex){
            System.out.println(integer + " was not found in " + counter + " iterations");
        }
    }
}
  
  