Grade:   95/100

Comments:

A) Does the code compile?  How can any compiler errors be resolved?

	Code compiles

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?

	If a number is given that is smaller than the previous, it enters an infinite loops of print statements

C) How can any runtime errors be resolved?

	Check for the exit conditions of the while loops

D) What topics should the student study in order to avoid the errors they made in this homework?

	While loops and exit conditions

E) Other comments:

	N/A 