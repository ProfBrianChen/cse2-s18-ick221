public class ComputeArea {
  public static void main(String[] args) {
    double radius; //declares radius
    double area; //declares area 
    //assigning a radius 
    radius = 20;
    //computing area
    area = radius * radius * 3.14159;
    //displaying radius 
    System.out.println("The area for the circle of radius" + radius + " is " + area);
    
  }
}

