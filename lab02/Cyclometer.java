//Izzy King Feb 2
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    int secsTrip1=480;  //time for trip 1 
    int secsTrip2=3220;  //time for trip 2
		int countsTrip1=1561;  //rotation counts for trip 1
		int countsTrip2=9037; //rotation counts for trip 2 
//our intermediate variables and output data
    double wheelDiameter=27.0;  //wheels diameter 
  	double PI=3.14159; //value for PI
  	int feetPerMile=5280;  //value for feet per mile
  	int inchesPerFoot=12;   //value for amount of inches per foot
  	int secondsPerMinute=60;  //valued for amount of seconds in a minute 
    double distanceTrip1, distanceTrip2,totalDistance;  //defining variables for each trip and the total distance 
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      //running the calculations and storing the values 
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
//Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class


