import java.util.Scanner;
public class Pyramid{ 
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in); //creates a scanner object and assigns its reference to the variable input 
    System.out.print("The square side of the pyramid is: ");//prompts user to enter value for side
    double Side= input.nextDouble();
    double width=Math.pow(Side, 2);
    System.out.print("The height of the pyramid is: ");//prompts user to enter height 
    double Height= input.nextDouble();//This statement reads a number from the keyboard and assigns the number to height 
    double volume;
    volume=((width*Height)/3);//computes volume 
    System.out.print("The volume inside the pyramid is:" +volume);//prints volume of pyramid 
  }
}

    