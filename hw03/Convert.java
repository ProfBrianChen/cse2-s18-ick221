import java.util.Scanner;
public class Convert{
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);//scanner allows us to modify variables with out recompiling whole code 
    System.out.print("Enter the affected area in acres: ");//prints affected area 
    double affectedArea=input.nextDouble();//assigns variable to affected area 
    System.out.print("Enter the rainfall in the affected area: "); //prints rainfall
    double rainfall=input.nextDouble(); //assigns variable to rainfall 
    double rainquantity=(rainfall*affectedArea);
    double milescubed=(rainquantity/40550400); //converts area of rain affected to cubic miles
    System.out.print("Total rainfall in cubic miles:" + milescubed); //prints final answer 
  }
}