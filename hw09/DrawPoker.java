import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

  public class DrawPoker {
  public static void main(String[] args) {
    
    int[] cards = new int[52]; //Deck of cards from 0 to 52 
    for (int j = 0; j < 52; j++) { //making sure all members of array are j 
      cards[j] = j; //setting each value 
    }
    

    shuffleDeck(cards); //shuffle deck 


    int[] playerOne = new int[5]; //hands to each player 
    int[] playerTwo = new int[5];
    for (int i = 0; i < 5; i++) {
      playerOne[i] = cards[2*i];
      playerTwo[i] = cards[2*i + 1];
    }

    System.out.println("Player 1's Hand: "); //prints out hands 
    for (int i : playerOne) {
      System.out.print(i + " ");
    }
    System.out.println("\n"); //formatting 

    System.out.println("Player 2's Hand: ");
    for (int i : playerTwo) {
      System.out.print(i + " ");
    }

    int player1Points = 0; //determines winner 
    int player2Points = 0;
    //Pairs
    if ( isPair(playerOne) ) {
      player1Points += 1;
    }

    if ( isPair(playerTwo) ) {
      player2Points += 1;
    }
    //Three of a Kind 
    if ( isThreeOfAKind(playerOne) ) {
      player1Points += 2;
    }

    if ( isThreeOfAKind(playerTwo) ) {
      player2Points += 2;
    }
    //Flushes
    if ( isFlush(playerOne) ) {
      player1Points += 4;
    }

    if ( isFlush(playerTwo) ) {
      player2Points += 4;
    }

    //FullHouse
    if ( isFullHouse(playerOne) ) {
      player1Points += 8;
    }

    if ( isFullHouse(playerTwo) ) {
      player2Points += 8;
    }
    
    //Compare if draw is the same 
    if (player1Points == player2Points) {
      if ( compareHighCard(playerOne, playerTwo) ) {
        player1Points += 1;
      }
      else {
        player2Points += 1;
      }
    }

    //Result
    if (player1Points > player2Points) {
       System.out.println("\n");
      System.out.println("Player 1 Wins!");
    }
    else {
       System.out.println("\n");
      System.out.println("Player 2 Wins!");
    }
  }

  public static void shuffleDeck(int[] input) {
    for (int i = 0; i < input.length; i++) {
      int random_num = (int) (Math.random() * input.length);
      int temp = input[i];
      input[i] = input[random_num];
      input[random_num] = temp;
    }
  }
    
  public static boolean isPair(int[] hand) {
    for (int i = 1; i < hand.length; i++) {
      if (hand[i] % 13 == hand[0] % 13 ) {
        return true;
      }
    } //end of for loops
    return false;
  }
    
  public static boolean isThreeOfAKind(int[] hand) {
    for (int i = 1; i < hand.length; i++) {
      for (int k = i + 1; k < hand.length; k++) {
        if (hand[0] % 13 == hand[i] % 13 && hand[0] % 13 == hand[k] % 13 ) {
          return true;
        }
      }
    } //end of for loops
    return false;
  }

  public static boolean isFlush(int[] hand) {
    int counter = 0;
    for (int i = 1; i < hand.length; i++) {
      if (hand[0] / 13 == hand[i] / 13) {
        counter += 1;
      }
    }
    if (counter == 4) {
      return true;
    }
    return false;
  }

  public static boolean isFullHouse(int[] hand) {
    if ( isPair(hand) && isThreeOfAKind(hand) ) {
      return true;
    }
    return false;
  }

  public static boolean compareHighCard(int[] hand1, int[] hand2) {
    int maxHand1 = hand1[0];
    for (int i = 0; i < hand1.length; i++) {
      if (hand1[i] > maxHand1) {
        maxHand1 = hand1[i];
      }
    }
    int maxHand2 = hand2[0];
    for (int i = 0; i < hand2.length; i++) {
      if (hand2[i] > maxHand2) {
        maxHand2 = hand2[i];
      }
    }
    if (maxHand1 > maxHand2) {
      return true;
    }
    return false;
  }
}
