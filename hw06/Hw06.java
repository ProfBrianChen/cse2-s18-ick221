import java.util.Scanner;
public class Hw06{
  //this line defines a main method 
  public static void main(String[] args) { 
    //ending main method 
   Scanner myScanner=new Scanner (System.in);
    System.out.println("Enter the Course Number "); //course number prompt 
    while (!myScanner.hasNextInt()){ //if input is not an integer the user will be prompted again 
      System.out.println("Error, Enter Integer"); //asks user to try again with an integer 
      myScanner.next(); //retrieves the integer typed by the user 
    }
    int courseNumber = myScanner.nextInt(); 
    
    //department name 
    System.out.println("Enter the Department Name ");
    while (!myScanner.hasNext()){
      System.out.println("Error, Enter String");
      myScanner.next();
    }
    String departmentName = myScanner.next();
    
    //number of times class meets per week 
    System.out.print("Enter Meeting Time per Week ");
    while(!myScanner.hasNextInt()){
      System.out.println("Error, Enter Integer");
      myScanner.next();
    }
    int meetingTime = myScanner.nextInt();
    
    //class start time 
    System.out.println("Enter the Class Start Time ");
    while(!myScanner.hasNextInt()){
      System.out.println("Error, Enter Integer");
      myScanner.next();
    }
    int startTime = myScanner.nextInt();
    
    //Instructor Name 
    System.out.println("Enter the Instructors Name ");
    while (!myScanner.hasNext()){
      System.out.print("Error, Enter String");
      myScanner.next();
    }
    String instructorName = myScanner.next();
    
    //number of students 
    System.out.println("Enter the Number of Studentes ");
    while(!myScanner.hasNextInt()){
      System.out.print("Error, Enter Integer");
      myScanner.next();
    }
    int numStudents = myScanner.nextInt();
    
     System.out.println("Course Number: " +courseNumber);
     System.out.println("Department Name: " +departmentName);
     System.out.println("Meeting Time Per Week: " +meetingTime);
     System.out.println("Class Start Time: " +startTime);
     System.out.println("Instructors Name: " +instructorName);
     System.out.println("Number of Students: " +numStudents);
    
  }
} 